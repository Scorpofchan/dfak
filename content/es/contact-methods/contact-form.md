---
layout: page
title: Formulario de Contacto
author: mfc
language: es
summary: métodos de contacto
date: 2018-09
permalink: /es/contact-methods/contact-form.md
parent: /es/
published: true
---

Lo más probable es que un formulario de contacto proteja tu mensaje hasta llegar a la organización receptora de tal modo que, solo tu y dicha organización puedan leerlo. Sin embargo, los gobiernos, organismos de seguridad u otros con el equipo técnico necesario podrían saber que al menos hubo contacto con la organización. Si deseas proteger mejor el hecho de que estás contactando a la organización, usa el navegador Tor para acceder al sitio web con el formulario de contacto.
