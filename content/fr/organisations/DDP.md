---
name: Digital Defenders Partnership
website: https://www.digitaldefenders.org
logo: DDP_logo_zwart_RGB.png
languages: Español, English, Français, Português, Deutsch, Nederlands, Русский
services: grants_funding, in_person_training, organisation_security, equipment_replacement, assessment, secure_comms, device_security, vulnerabilities_malware
beneficiaries: activists, journalists, hrd, lgbti, women, youth, cso
hours: Monday-Thursday 9am-5pm CET
response_time: 4 days
contact_methods: email, phone, mail
email: team@digitaldefenders.org
mail: Raamweg 16, 2596 HL Den Haag
phone: +31 070 376 5500
---

Le Digital Defenders Partnership (DDP) a pour objectif de protéger et de faire progresser la liberté de l'Internet et de le garder à l'abri des menaces émergentes, en particulier dans les environnements répressifs. Nous coordonnons l'aide d'urgence aux personnes et aux organisations telles que les défenseurs des droits humains, les journalistes, les militants de la société civile et les blogueurs. Nous adoptons une approche centrée sur les personnes, axée sur nos valeurs fondamentales de transparence, de droits de la personne, d'inclusivité et de diversité, d'égalité, de confidentialité et de liberté. DDP a été créé en 2012 par la Freedom Online Coalition (FOC).

Le DDP propose trois types différents de financement pour faire face aux situations d'urgence, ainsi que des subventions à plus long terme axées sur le renforcement des capacités au sein d'une organisation. De plus, nous coordonnons 
un programme d'accompagnement à la sécurité numérique (Digital Integrity Fellowship) où les organisations reçoivent des formations personnalisées à la sécurité et à la confidentialité numérique, articulé avec le programme de réponse rapide (Rapid Response Network).
