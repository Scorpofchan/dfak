---
name: Greenhost
website: https://greenhost.net
logo: greenhost.jpg
languages: English, Deutsch, Nederlands
services: org_security, web_hosting, web_protection, assessment, secure_comms, vulnerabilities_malware, browsing, ddos
beneficiaries: activists, lgbti, women, youth, cso
hours: 24/7, UTC+2
response_time: 4 hours
contact_methods: web_form, email, pgp, mail, phone
web_form: https://greenhost.net/about-us/contact/
email: support@greenhost.nl
pgp_key_fingerprint: 37CD 8929 D4F8 82B0 8F66 18C3 0473 77B4 B864 2066
phone: +31 20 489 04 44
mail: Johan van Hasseltkade 202, Amsterdam Noord, 1032 LP Amsterdam, Netherlands
initial_intake: yes
---

Greenhost offre des services informatiques avec une approche éthique et durable. 

Nos offres de services comprennent l'hébergement Web, les services de type cloud et de performantes offres spécifiques dans le domaine de la sécurité de l'information. En collaboration avec des organismes culturels et des pionniers de la technologie, nous nous efforçons d'offrir à nos utilisateurs toutes les possibilités de l'Internet tout en protégeant leur vie privée. 

Nous sommes activement impliqués dans le développement de logiciels libres et participons à divers projets dans les domaines de la technologie, du journalisme, de la culture, de l'éducation, de la durabilité et de la liberté sur Internet. 
