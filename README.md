Digital First Aid Kit
============

The [Digital First Aid Kit](https://rarenet.gitlab.io/dfak) aims at providing initial support for people facing the most common types of digital threats. *The Digital First Aid Kit is not meant to serve as the ultimate solution to all your digital emergencies.*

The [website](https://rarenet.gitlab.io/dfak/) deployed from this repository offers a series of [diagnostic workflows](content/en/topics/) for a series of frequent digital security issues that can be harder to analyze and, therefore, to address.

By following these workflows, the reader can analyze their issue step by step until they reach a diagnosis, with tips on how to address their digital security emergencies, or on whom they can reach out to to solve their issues.

The Digital First Aid Kit is addressed at rapid responders, digital security trainers, and activists who support their community in the digital self-defense sphere.


About
-----

The Digital First Aid Kit is a collaborative effort of a number of organizations and individuals in the [Rapid Response Community](https://rarenet.org) and beyond - see our [Contributors list](/CONTRIBUTORS.md).


## Repo structure

```
content/en
├── index
│   ├── intro.md
│   ├── ...
├── organisations
│   ├── access_now.md
│   └── greenhost.md
├── contact-methods
│   ├── email.md
│   ├── pgp.md
│   └── ...
└── topics
    ├── account-access-issues.md
    ├── device-acting-suspiciously.md
    ├── device-seized.md
    ├── harassed-online.md
    ├── website-not-working.md
    └── ...
```

 - The editorial content is in the `/content` folder.
 - Each language has the same structure as the above `/content/en`

The Digital First Aid Kit contains different kinds of content, with their specific format and structure:

- [Topics](content/en/topics) - Diagnostic workflows for addressing digital security emergencies
- [Contact Methods](content/en/contact-methods) - Description of the various contact methods accepted by the organizations listed in the website, and warnings on risks connected to their usage.
- [Organizations](content/en/organisations) - Member organizations of the Rapid Response Network that offer support in case of emergencies.


How to Contribute
--------------

 - Create a Gitlab account
 - Create a Fork
 - Edit files
 - Ensure that sites builds correctly at `https://MY_NAME.gitlab.io/dfak/` (change MY_NAME to your gitlab user account).
 - When work is ready submit a Merge Request
 - DFAK editors will review your changes and comment on your Merge Request, possibly asking you to make some edits.  
 - Once the MR is satisfying to all then it will be approved and merged.
 - You contribution is now accepted.

Please make sure to always fill in the meta tags in the YAML front matter, as this contains important information for the deployment of the website.

While the body of the files in the "Contact Methods" and "Organizations" folder should only contain a description of the contact methods or the organization respectively, the content of the "Topics" folder follows a concatenation of diagnostic steps that lead to different possible endings. What follows is a description of how topics are structured. Please read this carefully if you would like to contribute to the Topics section.


### Contribute Topics

The "Topics" folder contains interactive diagnostic workflows on several digital security issues that rapid responders encounter often in their work. Each diagnostic workflow consists in a concatenation of steps that start from a symptom (e.g. "I can't access the internet", or "I can't reach my website") and, based on the choices made in each screen, help the reader identify the nature of the issue they are facing and offer recommendations to address their issue. In case no diagnosis is reached, the reader is presented with a list of organizations that can help them mitigate their emergency. All workflows end with a series of tips to reduce the risk of the issue arising again.

To contribute a new topic, please follow this structure:

1. Introduction - one or two paragraphs that explain what the issue might be about.
2. Workflow - a series of questions aimed at identifying the cause of the issue, starting from the most urgent, for example to rule out a life-threatening situation or potential legal problems for the affected person.

    Answers to each question should either lead to a new question or to a section with recommendations.

    Sections with recommendations should end with feedback questions on whether the recommendations worked or not:
    
    - a "no" answer to this question should lead to:
        1. more questions in case the recommendations didn't work and a different diagnosis is still possible, or
        2. referrals to organizations specializing in the mitigation of the diagnosed problem
    - a "yes" answer to this question should lead to the final section with recommendations.

You can find an example of the syntax and of the organization of workflows in this [Workflow Template](workflow_template.md).


License
-------

Content is licensed as [Creative Commons BY-SA 4.0 License](https://creativecommons.org/licenses/by-sa/4.0/).
Code is licensed as [GNU General Public License version 3](https://www.gnu.org/copyleft/gpl.html) - {confirm}