source: content
clean: false
frontmatter: true
metadata:
  site:
    baseurl: "/@dfak/preview"
    code:
      current_branch: master
      base: "https://gitlab.com/"
      repo: "rarenet/dfak"
      package: ""

    # Site settings
    title: Digital First Aid Kit
    email: rarenet@rarenet.org
    description: > # this means to ignore newlines until "baseurl:"
      The Digital First Aid Kit aims to provide preliminary support for people facing the most common types of digital threats. The Kit offers a set of self-diagnostic tools for citizen, human rights defenders, bloggers, activists and journalists fac ing attacks themselves, as well as providing guidelines for digital first responders to assist a person under threat.
    # baseurl: /DFAK # Remove or edit when hosting outside of GH-pages; visit this subdir if running locally
    # twitter_username: ""
    author: CiviCERT
    github_username: RaReNet

    # The all languages used
    languages: ["en", "fr", "es", "ru", "ar", "pt"]
    # url: "https://digitalfirstaid.org" # the base hostname & protocol for your site

    # Nav and strings il8n
    strings:
      en:
        answer: "Now, answer the following questions to get a better idea of what is going on."
        start: "Start"
        previous_question: "Previous question"
        more_tips: "More tips"
        expand_info: "Expand info"
        fold: "Fold"
        find_support: "Find support"
        about: "About"
        self_care: "Self care"
        gitlab_repo: "Gitlab repo"
      fr:
        answer: "Maintenant, répondez aux questions suivantes pour avoir une meilleure idée de ce qui se passe."
        start: "Démarrer"
        previous_question: "Question précédente"
        more_tips: "Plus de conseils"
        expand_info: "Déplier les infos"
        fold: "Replier les infos"
        find_support: "Trouver de l'aide"
        about: "A propos"
        self_care: "Préserver son bien-être"
        gitlab_repo: "Dépôt Gitlab"
      es:
        answer: "Ahora, responde a las siguientes preguntas para tener una mejor idea de qué está sucediendo."
        start: "Comenzar"
        previous_question: "Pregunta anterior"
        more_tips: "Más consejos"
        expand_info: "Mostrar más información"
        fold: "Mostrar menos información"
        find_support: "Conseguir ayuda"
        about: "Acerca de"
        self_care: "Auto cuidado"
        gitlab_repo: "Repositorio en Gitlab"
      ru:
        answer: "Пожалуйста, ответьте на следующие вопросы для прояснения ситуации."
        start: "Начать"
        previous_question: "Предыдущий вопрос"
        more_tips: "Ещё подсказки"
        expand_info: "Развернуть"
        fold: "Свернуть"
        find_support: "Поиск поддержки"
        about: "О проекте"
        self_care: "Забота о себе"
        gitlab_repo: "Репозиторий Gitlab"
      ar:
        answer: "Now, answer the following questions to get a better idea of what is going on."
        start: "Start"
        previous_question: "Previous question"
        more_tips: "More tips"
        expand_info: "Expand info"
        fold: "Fold"
        find_support: "Find support ar"
        about: "About ar"
        self_care: "Self care ar"
        gitlab_repo: "Gitlab repo"
      pt:
        answer: "Now, answer the following questions to get a better idea of what is going on."
        start: "Start"
        previous_question: "Previous question"
        more_tips: "More tips"
        expand_info: "Expand info"
        fold: "Fold"
        find_support: "Find support pt"
        about: "About pt"
        self_care: "Self care pt"
        gitlab_repo: "Gitlab repo"

    # Tag i18n
    tags:
      en:
        services:
          triage: Initial Triage
          grants_funding: Grants & Funding
          in_person_training: In-Person Training
          org_security: Organizational Security
          web_hosting: Website Hosting
          web_protection: Website Protection (Denial of Service Protection)
          digital_support: 24/7 digital support
          relocation: Relocation
          physical_sec: Physical Security
          equipment_replacement: Equipment replacement
          assessment: Assessing Threats and Risks
          secure_comms: Securing Communications
          device_security: Device Security
          vulnerabilities_malware: Vulnerabilities and Malware
          browsing: Web Browsing Security
          account: Account Security
          harassment: Online Harassment Mitigation (Doxxing, Trolling)
          forensic: Forensic Analysis
          legal: Legal Support
          individual_care: Individual care
          advocacy: Public advocacy for individuals (arrested, in prison, etc.)
          ddos: Denial of Service Protection
          censorship: Censorship circumvention
        beneficiaries:
          journalists: Journalists
          hrds: Human Rights Defenders
          hros: Human rights organisations at risk working in their countries
          activists: Activists
          lgbti: LGBTI Groups
          women: Women
          youth: Youth
          cso: Civil Society Organisations
        methods:
          web_form: Web Form
          email: Email
          pgp: PGP
          pgp_key: Public Key URL
          pgp_key_fingerprint: PGP Key Fingerprint
          mail: Post
          signal: Signal
          skype: Skype
          whatsapp: WhatsApp
          phone: Phone
          tor: Tor
          telegram: Telegram
      fr:
        services:
          triage: Initial Triage
          grants_funding: Grants & Funding
          in_person_training: In-Person Training
          org_security: Organizational Security
          web_hosting: Website Hosting
          web_protection: Website Protection (Denial of Service Protection)
          digital_support: 24/7 digital support
          relocation: Relocation
          physical_sec: Physical Security
          equipment_replacement: Equipment replacement
          assessment: Assessing Threats and Risks
          secure_comms: Securing Communications
          device_security: Device Security
          vulnerabilities_malware: Vulnerabilities and Malware
          browsing: Web Browsing Security
          account: Account Security
          harassment: Online Harassment Mitigation (Doxxing, Trolling)
          forensic: Forensic Analysis
          legal: Legal Support
          individual_care: Individual care
          advocacy: Public advocacy for individuals (arrested, in prison, etc.)
          ddos: Denial of Service Protection
          censorship: Censorship circumvention
        beneficiaries:
          journalists: Journalists
          hrds: Human Rights Defenders
          hros: Human rights organisations at risk working in their countries
          activists: Activists
          lgbti: LGBTI Groups
          women: Women
          youth: Youth
          cso: Civil Society Organisations
        methods:
          web_form: Web Form
          email: Email
          pgp: PGP
          pgp_key: Public Key URL
          pgp_key_fingerprint: PGP Key Fingerprint
          mail: Post
          signal: Signal
          skype: Skype
          whatsapp: WhatsApp
          phone: Phone
          tor: Tor
          telegram: Telegram
      es:
        services:
          triage: Evaluación inicial
          grants_funding: Subvenciones y financiamiento
          in_person_training: Entrenamiento en persona
          org_security: Seguridad organizacional
          web_hosting: Alojamiento web
          web_protection: Protección de sitios web (Protección para Denegación de Servicio)
          digital_support: Soporte digital 24/7
          relocation: Relocalización
          physical_sec: Seguridad física
          equipment_replacement: Reemplazo de equipos
          assessment: Evaluación de amenazas y riesgos
          secure_comms: Comunicaciones seguras
          device_security: Seguridad de dispositivos
          vulnerabilities_malware: Vulnerabilidades y malware
          browsing: Seguridad de navegación web
          account: Seguridad de cuentas
          harassment: Mitigación de acoso online (Doxxing, Trolling)
          forensic: Análisis forense
          legal: Asistencia legal
          individual_care: Ciudado individual
          advocacy: Incidencia pública para individuales (situación de arresto, prisión, etc.)
          ddos: Protección de denegación de servicio
          censorship: Evasión de censura
        beneficiaries:
          journalists: Periodistas
          hrds: Defensores de Derechos Humanos
          hros: Organizaciones de Derechos Humanos en riesgo trabajando en sus países
          activists: Activistas
          lgbti: Grupos LGBTI
          women: Mujeres
          youth: Jóvenes
          cso: Organizaciones de la Sociedad Civil
        methods:
          web_form: Web Form
          email: Email
          pgp: PGP
          pgp_key: Public Key URL
          pgp_key_fingerprint: PGP Key Fingerprint
          mail: Post
          signal: Signal
          skype: Skype
          whatsapp: WhatsApp
          phone: Phone
          tor: Tor
          telegram: Telegram
      ru:
        services:
          triage: Первичная сортировка запросов
          grants_funding: Финансирование и гранты
          in_person_training: Персональное обучение
          org_security: Организационная безопасность
          web_hosting: Хостинг веб-сайтов
          web_protection: Защита веб-сайтов от DDoS-атак
          digital_support: Техподдержка 24/7
          relocation: Перемещение активистов
          physical_sec: Физическая безопасность
          equipment_replacement: Обновление оборудования
          assessment: Оценка рисков и построение модели угроз
          secure_comms: Безопасность коммуникаций
          device_security: Безопасность устройств
          vulnerabilities_malware: Уязвимости и вредоносный код
          browsing: Безопасность посещения сайтов
          account: Безопасность аккаунтов
          harassment: Борьба с онлайновыми домогательствами, доксингом, троллингом
          forensic: Экспертиза
          legal: Юридическая поддержка
          individual_care: Индивидуальная поддержка
          advocacy: Общественная защита частных лиц (арестованных, заключенных и др.)
          ddos: Защита от DDoS-атак
          censorship: Обход цензуры
        beneficiaries:
          journalists: Журналисты
          hrds: Правозащитники
          hros: Правозащитные организации из группы риска, работающие в своих странах
          activists: Активисты
          lgbti: Группы ЛГБТ+
          women: Женщины
          youth: Молодёжь
          cso: Организации гражданского общества
        methods:
          web_form: Веб-форма
          email: Email
          pgp: PGP
          pgp_key: Адрес открытого ключа
          pgp_key_fingerprint: Отпечаток ключа PGP
          mail: Почта
          signal: Signal
          skype: Skype
          whatsapp: WhatsApp
          phone: Телефон
          tor: Tor
          telegram: Telegram
      ar:
        services:
          triage: Initial Triage
          grants_funding: Grants & Funding
          in_person_training: In-Person Training
          org_security: Organizational Security
          web_hosting: Website Hosting
          web_protection: Website Protection (Denial of Service Protection)
          digital_support: 24/7 digital support
          relocation: Relocation
          physical_sec: Physical Security
          equipment_replacement: Equipment replacement
          assessment: Assessing Threats and Risks
          secure_comms: Securing Communications
          device_security: Device Security
          vulnerabilities_malware: Vulnerabilities and Malware
          browsing: Web Browsing Security
          account: Account Security
          harassment: Online Harassment Mitigation (Doxxing, Trolling)
          forensic: Forensic Analysis
          legal: Legal Support
          individual_care: Individual care
          advocacy: Public advocacy for individuals (arrested, in prison, etc.)
          ddos: Denial of Service Protection
          censorship: Censorship circumvention
        beneficiaries:
          journalists: Journalists
          hrds: Human Rights Defenders
          hros: Human rights organisations at risk working in their countries
          activists: Activists
          lgbti: LGBTI Groups
          women: Women
          youth: Youth
          cso: Civil Society Organisations
        methods:
          web_form: Web Form
          email: Email
          pgp: PGP
          pgp_key: Public Key URL
          pgp_key_fingerprint: PGP Key Fingerprint
          mail: Post
          signal: Signal
          skype: Skype
          whatsapp: WhatsApp
          phone: Phone
          tor: Tor
          telegram: Telegram
      pt:
        services:
          triage: Initial Triage
          grants_funding: Grants & Funding
          in_person_training: In-Person Training
          org_security: Organizational Security
          web_hosting: Website Hosting
          web_protection: Website Protection (Denial of Service Protection)
          digital_support: 24/7 digital support
          relocation: Relocation
          physical_sec: Physical Security
          equipment_replacement: Equipment replacement
          assessment: Assessing Threats and Risks
          secure_comms: Securing Communications
          device_security: Device Security
          vulnerabilities_malware: Vulnerabilities and Malware
          browsing: Web Browsing Security
          account: Account Security
          harassment: Online Harassment Mitigation (Doxxing, Trolling)
          forensic: Forensic Analysis
          legal: Legal Support
          individual_care: Individual care
          advocacy: Public advocacy for individuals (arrested, in prison, etc.)
          ddos: Denial of Service Protection
          censorship: Censorship circumvention
        beneficiaries:
          journalists: Journalists
          hrds: Human Rights Defenders
          hros: Human rights organisations at risk working in their countries
          activists: Activists
          lgbti: LGBTI Groups
          women: Women
          youth: Youth
          cso: Civil Society Organisations
        methods:
          web_form: Web Form
          email: Email
          pgp: PGP
          pgp_key: Public Key URL
          pgp_key_fingerprint: PGP Key Fingerprint
          mail: Post
          signal: Signal
          skype: Skype
          whatsapp: WhatsApp
          phone: Phone
          tor: Tor
          telegram: Telegram

plugins:
  - metalsmith-env:
      variables:
        CI: false

  - metalsmith-drafts: {}

  - metalsmith-metadata:
      site: _config.yml

  - metalsmith-dynamic-collections:
      pages:
        pattern: "*/*.?(md|html)"
      topics:
        pattern: "*/topics/*.?(md|html)"
      organisations:
        sortBy: "title"
        pattern: "*/organisations/*.?(md|html)"
      methods:
        pattern: "*/contact_methods/*.?(md|html)"
      posts:
        pattern: "_posts/*.md"

  # Add language metadata
  - metalsmith-filemetadata:
      - pattern: "en/**/*.md"
        preserve: "true"
        metadata:
          language: en
      - pattern: "es/**/*.md"
        preserve: "true"
        metadata:
          language: es
      - pattern: "fr/**/*.md"
        preserve: "true"
        metadata:
          language: fr
      - pattern: "ru/**/*.md"
        preserve: "true"
        metadata:
          language: ru
      - pattern: "ar/**/*.md"
        preserve: "true"
        metadata:
          language: ar
      - pattern: "pt/**/*.md"
        preserve: "true"
        metadata:
          language: pt

  - metalsmith-markdown-parse:
      pattern: "**/topics/*.md"
      title: "Workflow"
      replace: |-
        <div id="startpara" class="pure-g"><div class="pure-u-1 pure-u-md-3-4"><p>Now, answer the following questions to get a better idea of what is going on.</p></div><div class="pure-u-1 pure-u-md-1-4"><a id="startbutton" href="$start">Start</a></div></div>

  - metalsmith-title:
      remove: false

  - metalsmith-paths:
      property: paths
      directoryIndex: index.html

  # Copy client side assets
  - metalsmith-assets-copy:
      replace: all
      src: code/assets
      dest: .

  - metalsmith-metacopy:
      file:
        - src: paths.href
          dest: url
        - src: paths.name
          dest: basename
        - src: redirect_from
          dest: aliases
        - src: contents
          dest: content
      metadata:
        - src: collections
          dest: site

  # Copy source markdown file link
  - metalsmith-metacopy:
      file:
        - src: paths.href
          dest: source

  # Add metadata
  - metalsmith-filemetadata:
      # - pattern: "*/**/*.md"
      #   # preserve: 'true'
      #   metadata:
      #     layout: page.pug
      - pattern: "*/**/topics/*.md"
        # preserve: 'true'
        metadata:
          layout: topic.pug
      - pattern: "*/**/topics/*/questions/*.md"
        # preserve: 'true'
        metadata:
          layout: question.pug
      - pattern: "*/raw.md"
        # preserve: 'true'
        metadata:
          layout: none.pug
      - pattern: "index.html.pug"
        preserve: "true"
        metadata:
          source: "/index.html.pug"

  - metalsmith-in-place:
      engine: pug
      pattern: "**/*.html.pug"

  - metalsmith-pandoc:
      pattern: "**/*.md"
      from: markdown_github+definition_lists+markdown_in_html_blocks
  #
  # - metalsmith-sass:
  #     sources: './code/css'
  #     outputDir: './css/'
  #     outputStyle: "expanded"

  - metalsmith-permalinks:
      linksets:
        - match:
            collection: pages
            path: _pages/index.html
          pattern: "./"
        - match:
            collection: pages
          pattern: ":permalink"
        - match:
            collection: topics
          pattern: ":permalink"
        - match:
            collection: posts
          pattern: ":basename/:title"

  - metalsmith-debug: {}

  - metalsmith-layouts:
      engine: pug
      directory: code/templates
      pattern: "**/*.html"
      # rename: true
      default: none.pug
      includes: includes
      # ext: ".html"
      cache: false
  #
  # - metalsmith-aliases:
  #     redirect: true
